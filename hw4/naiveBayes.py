
# coding: utf-8

# ### Naive Bayes, CS3001 HW4
# Henry C. Wong

# In[1]:


import pandas as pd
import numpy as np
from sklearn.naive_bayes import GaussianNB
from sklearn import metrics


# In[2]:


train = pd.read_csv("train.csv")
test = pd.read_csv("test.csv")


# In[3]:


train.info()


# In[4]:


train.head()


# ### Data Wrangling

# In[5]:


train['label'] = train['label'].map({'lose':0,'win':1})
test['label'] = test['label'].map({'lose':0,'win':1})


# In[6]:


train['is_Opponent_in_AP25_Preseason'] = train['is_Opponent_in_AP25_Preseason'].map({'out':0,'in':1})
test['is_Opponent_in_AP25_Preseason'] = test['is_Opponent_in_AP25_Preseason'].map({'out':0,'in':1})


# In[7]:


train['is_Home_or_Away'] = train['is_Home_or_Away'].map({'home':0,'away':1}) 
test['is_Home_or_Away'] = test['is_Home_or_Away'].map({'home':0,'away':1}) 


# In[8]:


train['opponent'].nunique()


# In[9]:


temp = {}
i=0


# Dynamically Allocated values to schools.
# I could also just change it in notepad, but I wanted practice.

# In[10]:


for school in train['opponent']:
    if school not in temp:
        temp[school] = i
        i += 1
for school in test['opponent']:
    if school not in temp:
        temp[school] = i
        i += 1
print(temp)


# In[11]:


train['opponent'] = train['opponent'].map(temp)
test['opponent'] = test['opponent'].map(temp)


# Seperating the win/lose condition from the data

# In[12]:


trainY = train['label']
testY = test['label']
train = train.drop(['label'],axis=1)
test = test.drop(['label'],axis=1)


# In[13]:


train = train.drop(['date'],axis=1)
test = test.drop(['date'],axis=1)


# ### Naive Bayes
# 
# **train** is the training data
# **trainY** is the win/lose label for the training data
# **test** is the test data
# **testY** is the win/lose label for the test data
# **y_pred** is the prediction for the test set using the Gaussian Naive Bayes Model 

# In[14]:


clf= GaussianNB()
y_pred = clf.fit(train,trainY).predict(test)
print(y_pred) #reminder: 0:lose, 1:win


# Comparing y_pred with the actual label values

# In[15]:


print("Number of mislabeled points out of a total %d points : %d" % (test.shape[0],(testY != y_pred).sum()))


# In[16]:


print("Classification report for classifier %s:\n%s\n"
      % (clf, metrics.classification_report(testY, y_pred)))

