
# coding: utf-8

# In[242]:


from sklearn.datasets.california_housing import fetch_california_housing
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn import linear_model
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import GridSearchCV
import numpy as np
from sklearn.metrics import accuracy_score


# In[19]:


a = fetch_california_housing()


# In[20]:


x = a.data[10000]


# In[5]:


x


# In[6]:


sns.distplot(x)


# In[14]:


a.feature_names


# In[15]:


a.target


# In[16]:


a.data


# In[31]:


plt.scatter(a.data.T[0],a.target, alpha=0.2)
plt.xlabel(a.feature_names[0])
plt.ylabel("House(Target) Value")


# In[32]:


plt.scatter(a.data.T[1],a.target, alpha=0.2)
plt.xlabel(a.feature_names[1])
plt.ylabel("House(Target) Value")


# In[33]:


plt.scatter(a.data.T[2],a.target, alpha=0.2)
plt.xlabel(a.feature_names[2])
plt.ylabel("House(Target Value)")


# In[34]:


plt.scatter(a.data.T[3],a.target, alpha=0.2)
plt.xlabel(a.feature_names[3])
plt.ylabel("House(Target Value)")


# In[35]:


plt.scatter(a.data.T[4],a.target, alpha=0.2)
plt.xlabel(a.feature_names[4])
plt.ylabel("House(Target Value)")


# In[36]:


plt.scatter(a.data.T[5],a.target, alpha=0.2)
plt.xlabel(a.feature_names[5])
plt.ylabel("House(Target Value)")


# In[37]:


plt.scatter(a.data.T[6],a.target, alpha=0.2)
plt.xlabel(a.feature_names[6])
plt.ylabel("House(Target Value)")


# In[38]:


plt.scatter(a.data.T[7],a.target, alpha=0.2)
plt.xlabel(a.feature_names[7])
plt.ylabel("House(Target Value)")


# ### Split bewteen Train and Test

# In[126]:


X_train,X_test,y_train,y_test = train_test_split(a.data,a.target,test_size=0.2,random_state=1)


# In[127]:


scaled_X_Train = StandardScaler().fit_transform(X_train,y_train)


# In[147]:


scaled_X_Test = StandardScaler().fit_transform(X_test,y_test)


# In[165]:


parameters = {'kernel':('linear', 'rbf'), 'C':[1,0.1,0.01,0.001,0.0001,0]}


# ##### Linear Regression

# In[129]:


reg = linear_model.LinearRegression().fit(X_train, y_train)


# In[130]:


reg.score(X_test,y_test)


# In[131]:


reg2 = linear_model.LinearRegression().fit(scaled_X_Train, y_train)


# In[132]:


reg2.score(scaled_X_Test,y_test)


# In[178]:





# In[161]:





# ##### Ridge Regression

# In[133]:


ridge = linear_model.Ridge().fit(X_train,y_train)


# In[134]:


ridge.score(X_test,y_test)


# In[135]:


ridge2 = linear_model.Ridge().fit(scaled_X_Train,y_train)


# In[136]:


ridge2.score(scaled_X_Test,y_test)


# In[185]:


linear_model.Ridge().get_params()


# Grid Search

# In[188]:


params = linear_model.Ridge().get_params()


# In[228]:


alphasRidge = np.array([1,0.1,0.01,0.001,0.0001,0])


# In[194]:


grid_ridge = GridSearchCV(linear_model.Ridge(),param_grid=dict(alpha=alphasRidge)).fit(X_train,y_train)


# In[195]:


grid_ridge.score(X_test,y_test)


# ##### Lasso

# In[219]:


lasso = linear_model.Lasso().fit(X_train,y_train)


# In[220]:


lasso.score(X_test,y_test)


# In[221]:


lasso2 = linear_model.Lasso().fit(scaled_X_Train,y_train)


# In[222]:


lasso2.score(scaled_X_Test,y_test)


# Grid Search

# In[223]:


linear_model.Lasso().get_params()


# In[224]:


alphasLasso = np.array([.001,.01,.1,1,10,100])


# In[225]:


grid_lasso= GridSearchCV(linear_model.Lasso(),param_grid=dict(alpha=alphasLasso)).fit(X_train,y_train)


# In[226]:


grid_lasso.score(X_test,y_test)


# ##### ElasticNet

# In[141]:


elasticNet = linear_model.ElasticNet().fit(X_train,y_train)


# In[142]:


elasticNet.score(X_test,y_test)


# In[122]:


elasticNet2 = linear_model.ElasticNet().fit(scaled_X_Train,y_train)


# In[123]:


elasticNet2.score(scaled_X_Test,y_test)


# Grid Search

# In[216]:


grid_elasticNet = GridSearchCV(linear_model.ElasticNet(),param_grid=dict(alpha=alphasLasso)).fit(X_train,y_train)


# In[217]:


grid_elasticNet.score(X_test,y_test)


# ## Q4

# In[246]:


ridgeCVChanges = {}
for x in range(5):
    grid_ridge = GridSearchCV(linear_model.Ridge(),param_grid=dict(alpha=alphasRidge),cv=x+2).fit(X_train,y_train)
    ridgeCVChanges[x+2] = grid_ridge.score(X_test,y_test)


# In[239]:


lassoCVChanges = {}
for x in range(5):
    grid_lasso = GridSearchCV(linear_model.Lasso(),param_grid=dict(alpha=alphasLasso),cv=x+2).fit(X_train,y_train)
    lassoCVChanges[x+2] = grid_lasso.score(X_test,y_test)


# In[263]:


elasticNetCVChanges = {}
for x in range(5):
    grid_elasticNet = GridSearchCV(linear_model.ElasticNet(),param_grid=dict(alpha=alphasLasso)).fit(X_train,y_train)
    elasticNetCVChanges[x+2] = grid_elasticNet.score(X_test,y_test)


# In[264]:


sns.barplot(list(ridgeCVChanges.keys()),list(ridgeCVChanges.values()))


# In[265]:


sns.barplot(list(lassoCVChanges.keys()),list(lassoCVChanges.values()))


# In[266]:


sns.barplot(list(elasticNetCVChanges.keys()),list(elasticNetCVChanges.values()))


# ## Q5

# In[269]:




