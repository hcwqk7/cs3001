# -*- coding: utf-8 -*-
"""
Created on Sun Nov 18 13:57:39 2018

@author: cywon
"""

import pandas as pd
import numpy as np
from nltk.cluster.kmeans import KMeansClusterer
from scipy.spatial import distance

def taxi_distance(u,v):
    print(u)
    print(v)
    diff = u-v
    return abs(np.dot(diff,diff))
  
dataMatrix = np.loadtxt(open("data.csv", "rb"), delimiter=",", skiprows=1)

NUM_CLUSTERS = 2
data = dataMatrix

kclusterer = KMeansClusterer(NUM_CLUSTERS, distance=taxi_distance,initial_means=[[3,2],[2,3]])
assigned_clusters = kclusterer.cluster(data, assign_clusters=True)