# -*- coding: utf-8 -*-
"""
Created on Sun Nov  4 15:51:35 2018

@author: Henry Wong
"""

import pandas as pd
import pickle
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.utils import shuffle
from sklearn.metrics import f1_score


def kFoldCV(df,k):
    """ Return a train and test pandas dataFrame
        
        Args
        ----
        df -- pandas Data Frame
        k -- k number of cross validation sets for k-fold cross validation
        
        Returns
        ----
        train -- a list of Data Frames, contain the different cross validation portions based on k
        test  -- a test set Data Frame, it's the last portion baed on the given k
    """
    splitBy = int(len(df.index)/k)
    train = []
    dfc = df.copy()
    dfc = shuffle(dfc)  # need to randomize
    for i in range(k-1):
        dropped = dfc.iloc[:splitBy]
        dfc = dfc.drop(dfc.head(splitBy).index)
        train.append(dropped)
    return(train,dfc)
  
def encode_target(df, target_column):
    """Add column to df with integers for the target.

    Args
    ----
    df -- pandas Data Frame.
    target_column -- column to map to int, producing new
                     Target column.

    Returns
    -------
    df -- modified Data Frame.
    targets -- list of target names.
    """
    df_mod = df.copy()
    targets = df_mod[target_column].unique()
    map_to_int = {name: n for n, name in enumerate(targets)}
    df_mod["Target"] = df_mod[target_column].replace(map_to_int)

    return (df_mod, targets)
  
df = pd.read_csv("iris.csv")
features = ["SepalLength", "SepalWidth",
            "PetalLength", "PetalWidth"]
df,targets = encode_target(df,"Name")
train,test = kFoldCV(df,5)

###############
# Decision Tree
###############

clf = DecisionTreeClassifier()
scores = []
f1Scores = []
for x in train:
  X = x[features]
  y = x["Target"]
  clf.fit(X,y)
  score = clf.score(test[features],test["Target"])
 # print(clf.predict(test[features]))
  f1Score = f1_score(test["Target"],clf.predict(test[features]),average="weighted")
  scores.append(score)
  f1Scores.append(f1Score)
cv5Train = train[:4]
cv5TrainCombined = pd.DataFrame()
for d in cv5Train:
  cv5TrainCombined = cv5TrainCombined.append(d)
clf.fit(cv5TrainCombined[features],cv5TrainCombined["Target"])
s = pickle.dump(clf,open("decisionTree.pickle","wb"))


averageScore = sum(scores) / len(scores)
averageF1 = sum(f1Scores) / len(f1Scores)
print("Average Score:", averageScore)
print("Average F1-Score:", averageF1)
outF = open("testing.txt","w")
outF.write("Average Decision Tree Score: %d \n" % averageScore)
outF.write("Average Decision Tree F1-Score: %d \n" % averageF1)

#############
# Knn
#############
scoresKNN = []
bestKNN = []
bestKNNScore = 0
f1KNNScores = []
for x in train:
  neigh = KNeighborsClassifier()
  X = x[features]
  y = x["Target"]
  neigh.fit(X,y)
  scoreKNN = neigh.score(test[features],test["Target"])
  f1KNNScore = f1_score(test["Target"],neigh.predict(test[features]),average="weighted")
  scoresKNN.append(scoreKNN)
  f1KNNScores.append(f1KNNScore)
averageKNNF1Score = sum(f1KNNScores)/len(f1KNNScores)
averageKNNScore = sum(scoresKNN)/len(scoresKNN)
print("Average KNN Score:", averageKNNScore)
print("Average KNN F1-Score:",averageKNNF1Score)
outF.write("Average KNN Score:%d \n" % averageKNNScore)
outF.write("Average KNN F1-Score:%d \n" % averageKNNF1Score)
outF.close()